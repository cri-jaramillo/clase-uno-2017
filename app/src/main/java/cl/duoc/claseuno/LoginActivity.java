package cl.duoc.claseuno;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnSaludo;
    private EditText etSaludo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnSaludo = (Button) findViewById(R.id.btnSaludo);
        etSaludo = (EditText) findViewById(R.id.etSaludo);

        /* btnSaludo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "Hola Mundo", Toast.LENGTH_LONG).show();
            }
        });*/

       btnSaludo.setOnClickListener(this);

    }

   @Override
    public void onClick(View v) {
        Toast.makeText(LoginActivity.this, "Hola "+etSaludo.getText().toString(), Toast.LENGTH_SHORT).show();
    }
}
